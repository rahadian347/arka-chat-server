'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('Users', [
      {
        id: 1, 
        name: 'Rahadian Permana',
        avatar: "https://i.ibb.co/d2jWNRv/profile-Picture.jpg",
        email: "rahadian@email.com",
        password: "1234",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: 2,
        name: 'Maudi Ayunda',
        avatar: "https://cdn1-production-images-kly.akamaized.net/sw8HL4DSfP6TX4l2knNNyRJk2Hg=/1x206:1080x814/680x383/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/2734250/original/028049100_1550718449-49612935_619565475141888_3590286969263794281_n.jpg",
        email: "maudy@email.com",
        password: "1234",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ], {})
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};

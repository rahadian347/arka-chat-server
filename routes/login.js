const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const saltRounds = 10
const user = require('../models').User

router.post('/', (req, res, next) => {
    let identity = {
        email: req.body.email,
        password: req.body.password
    }

    user.findOne({
        where: identity
    })
        .then(data => {
            if (!(data)) {
                res.status(403).json({
                    message: 'authentication failed',
                    status: 403
                })
            } else {
                payload = data.dataValues
                console.log(payload)
                password = payload.password

                delete payload.password
                delete payload.createdAt
                delete payload.updatedAt

                let token = jwt.sign(payload, process.env.SECRET_KEY, { expiresIn: '2h' })
                console.log(token)
                res.status(200).json({
                    data: payload,
                    token: token,
                    status: 200
                })

            }
        })
        .catch(err => {
            res.status(500).send('Internal Server Error coy')
        })
})

module.exports = router;
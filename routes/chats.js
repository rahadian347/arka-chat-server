var express = require('express');
const user = require('../models').User
const chat = require('../models').Chat
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
    chat.findAll({
        include: user,
        order: [
            ['createdAt', 'DESC']
        ],
    })
        .then(data => {
         
           res.status(200).json(data)
        })
        .catch(err => {
            res.sendStatus(500).json({
                message: "Server error: " + err,
            })
        })
})

router.post('/', (req, res, next) => {

    let body = {
        user_id: req.body.user_id,
        text: req.body.text

    }
    console.log(body)
    if (!body.text || !body.user_id) {
        res.status(400).json({
            message: "Bad Request"
        })
    } else {
        chat.create(body)
            .then(data => {
                res.status(201).json(data.dataValues)
            })
            .catch(err => {
                console.log(err)
                res.status(500).json({
                    message: `internal server error`,
                    status: 500
                })
            })
    }
})

module.exports = router;
